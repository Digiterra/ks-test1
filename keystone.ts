import { config } from '@keystone-6/core';
import { lists } from './schema';
import { insertPosts, insertTree } from './generateTree';
import 'dotenv/config'

export default config({
  db: {
    provider: 'sqlite',
    url: process.env.DATABASE_URL || 'file:./keystone-example.db',
    async onConnect(context) {
      if (process.argv.includes('-p')) {
        await insertPosts(context);
      }
      if (process.argv.includes('-t')) {
        await insertTree(context);
      }
    },
  },
  server: {
    port: parseInt(process.env.APP_PORT)
  },
  lists,
});
