## Nested set test

How to test:

1. Comment the `tree: nestedSet(),` line in the schema.ts
2. Generate posts without the tree via command `yarn dev -p`
3. Uncomment the `tree: nestedSet(),` line in the schema.ts
4. Generate initial tree via command `yarn dev -t`
5. Start keystone using `yarn dev`
6. Click on "+ Post" button and see the tree in dropdown.
7. Check that the tree matches to the generated structure. Generator is in file `generateTree.ts`.
8. Do various oprations with tree and check if always is done ok.
