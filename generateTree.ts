import { KeystoneContext } from '@keystone-6/core/types';

export async function insertPosts(context: KeystoneContext) {
  console.log(`🌱 Inserting posts`);

  const p1 = await context.query.Post.createOne({
    data: {title: "p1"},
    query: 'id',
  });
  console.log(`Inserted post`, p1);

  const p2 = await context.query.Post.createOne({
    data: {title: "p2"},
    query: 'id',
  });
  console.log(`Inserted post`, p2);

  const p3 = await context.query.Post.createOne({
    data: {title: "p3"},
    query: 'id',
  });
  console.log(`Inserted post`, p3);

  console.log(`✅ Post data inserted`);
  process.exit();
}


export async function insertTree(context: KeystoneContext) {
  console.log(`🌱 Inserting tree`);

  const p1 = (await context.query.Post.findMany({
    where: { title: { equals: "p1" } }
  }))[0];
  const p2 = (await context.query.Post.findMany({
    where: { title: { equals: "p2" } }
  }))[0];
  const p3 = (await context.query.Post.findMany({
    where: { title: { equals: "p3" } }
  }))[0];

  const troot = await context.query.Post.createOne({
    data: {title: "troot"},
    query: 'id',
  });

  const t_1 = await context.query.Post.createOne({
    data: {title: "t_1", tree: { parentId: troot.id }},
    query: 'id',
  });
  const t_3 = await context.query.Post.createOne({
    data: {title: "t_3", tree: { parentId: troot.id }},
    query: 'id',
  });
  const t_2 = await context.query.Post.createOne({
    data: {title: "t_2", tree: { nextSiblingOf: t_1.id }},
    query: 'id',
  });
  const t_1_1 = await context.query.Post.createOne({
    data: {title: "t_1_1", tree: { parentId: t_1.id }},
    query: 'id',
  });
  const t_1_2 = await context.query.Post.createOne({
    data: {title: "t_1_2", tree: { parentId: t_1.id }},
    query: 'id',
  });
  const t_1_1_1 = await context.query.Post.createOne({
    data: {title: "t_1_1_1", tree: { parentId: t_1_1.id }},
    query: 'id',
  });

  const t_1_1_2 = await context.query.Post.createOne({
    data: {title: "t_1_1_2", tree: { parentId: t_1_1.id }},
    query: 'id',
  });

  const t_1_1_3 = await context.query.Post.createOne({
    data: {title: "t_1_1_3", tree: { parentId: t_1_1.id }},
    query: 'id',
  });

  console.log(`✅ Tree data inserted`);

  console.log(`🌱 Starting Mutations`);

  await context.query.Post.updateOne({
    where: { id: p1.id },
    data: { tree: { parentId: t_1_2.id } },
  });
  await context.query.Post.updateOne({
    where: { id: p3.id },
    data: { tree: { nextSiblingOf: p1.id } },
  });
  await context.query.Post.updateOne({
    where: { id: p2.id },
    data: { tree: { prevSiblingOf: p3.id } },
  });

  await context.query.Post.updateOne({
    where: { id: t_1.id },
    data: { tree: { parentId: t_2.id } },
  });

  console.log(`✅ Mutations executed`);
  process.exit();

}
